#include "XMainWindow.h"

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	if (!g_pMainWindow)
		g_pMainWindow = new CXMainWindow(hInstance, L"Title", L"MainWindow001", 125, 125, 500, 500);

	g_pMainWindow->SetBgColor(RGB(25, 25, 25));

	if (!g_pMainWindow->Create())
		return -1;

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0) > 0)
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}