#pragma once
#include "XWindow.h"
#include "XControls.h"

class CXMainWindow : public CXWindow
{
public:
	CXMainWindow(HINSTANCE hInstance, tstring szTitle, tstring szClass, int x, int y, int w, int h);
	~CXMainWindow();

	bool Create();
	
	void OnPaint();
	LRESULT OnEraseBkgrnd(WPARAM wParam);

	CXControls* pControls = nullptr;
	CXControlGroup RadioGroup;
private:
	bool bUpdate = true;
};

extern CXMainWindow * g_pMainWindow;