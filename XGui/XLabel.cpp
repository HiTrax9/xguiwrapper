#include "XLabel.h"



CXLabel::CXLabel(CXWindow * pOwner, XID xID, int x, int y, int w, int h, tstring szText, CXFont * pFont)
	: CXControl(pOwner, xID, LABEL, x, y, w, h, szText, pFont)
{
	szClass = _T("STATIC");
	SetStyle(WS_XLABEL);
}


CXLabel::~CXLabel()
{
}/*

bool CXLabel::Create()
{
	bool bRet = CXControl::Create();
	if (bRet)
	{
		SetTxtColor(RGB(255, 0, 0));
		Static_SetText(hWnd, szText.c_str());
	}
	return bRet;
}*/

LRESULT CXLabel::OnDrawItem(WPARAM wParam, LPARAM lParam)
{
	DRAWITEMSTRUCT* pDi = (DRAWITEMSTRUCT*)lParam;
	SetTextColor(pDi->hDC, clText);
	SetBkMode(pDi->hDC, TRANSPARENT);
	if (pFont)
		pFont->SetFont(pDi->hDC);
	DrawText(pDi->hDC, szText.c_str(), szText.length(), &GetRect(), DT_NOCLIP);
	return 0;
}
