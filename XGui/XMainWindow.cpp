#include "XMainWindow.h"
#define IDEDIT1			0x7000
#define IDBUTTON1		0x8000
#define IDLABEL1		0x8050
#define IDRADIO1		0x9000
#define IDRADIO2		0x9001
#define IDCHECK1		0x9901
#define IDLISTBOX0		0x7901
#define IDCOMBO0		0x9999

#define IDTABCONTROL0	0x5550
#define IDGROUP0RADIO0	0x9550
#define IDTRACKBAR0		0x6666
#define IDSYSLINK0		0x3333

CXMainWindow * g_pMainWindow = nullptr;
CXControls * g_pControls = nullptr;
CXFont * xAgency = nullptr;
CXFont * xVerdana = nullptr;

CXRadioButton * pRadio = nullptr;

void Button1Func(int num, tstring szText)
{
	for (int x = 0; x < num; x++)
	{
		MessageBox(NULL, szText.c_str(), L"2Boxes?", MB_OK);
	}
	g_pControls->mControls[IDBUTTON1]->SetText(L"New Text!");
}

struct B1S
{
	int n;
	tstring s;
};

void B1Wrap(uintptr_t a1)
{
	B1S b = *(B1S*)a1;
	b.s = g_pControls->GetControl<CXEdit>(IDEDIT1)->GetText();
	Button1Func(b.n, b.s);
	g_pControls->GetControl<CXEdit>(IDEDIT1)->SetText(L"semen");
}

BOOL WINAPI EnumChildProc(HWND hWnd, LPARAM lParam);

bool bIsTracked = false;
LRESULT CALLBACK MainWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
		case WM_CREATE:
		{
			if (g_pControls)
			{
				CXButton * pButton = g_pControls->GetControl<CXButton>(IDBUTTON1);
				//if (pButton)
				//{
				//	pButton->SetText(_T("Changed"));
				//}
			}
			break;
		}
		case WM_COMMAND:
		{
			int id = LOWORD(wParam);
			g_pControls->mControls[id]->OnCommand(wParam, lParam);
			return 0;
		}
		case WM_DRAWITEM:
		{
			if (g_pControls)
				return g_pControls->mControls[wParam]->OnDrawItem(wParam, lParam);
			break;
		}
		case WM_PAINT:
		{
			g_pMainWindow->OnPaint();
			break;
		}
		case WM_CTLCOLOR:
		case WM_CTLCOLORBTN:
		case WM_CTLCOLORSTATIC:
		{
			int id = GetDlgCtrlID((HWND)lParam);
			return g_pControls->mControls[id]->OnCtlColor(wParam, lParam);
			//return LRESULT(GetStockObject(HOLLOW_BRUSH));
		}
		case WM_NOTIFY:
		{
			NMHDR * pNMHDR = (NMHDR*)lParam;

			int id = pNMHDR->idFrom;
			if(id > 1)
			g_pControls->mControls[id]->OnNotify(pNMHDR->code, lParam);


			break;
		}
		case WM_MOUSEMOVE:
		{
			break;
		}
		case WM_MOUSEHOVER:
		{
			break;
		}
		case WM_MOUSELEAVE:
		{
			break;
		}
		case WM_DESTROY:
		{
			DestroyWindow(g_pMainWindow->GetHandle());
		}
		case WM_CLOSE:
		{
			PostQuitMessage(0);
		}
	}
	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

BOOL WINAPI EnumChildProc(HWND hWnd, LPARAM lParam)
{
	POINT p;
	p.x = LOWORD(lParam);
	p.y = HIWORD(lParam);
	int id = GetDlgCtrlID(hWnd);
	g_pControls->mControls[id]->OnHover(p);
	RedrawWindow(g_pControls->mControls[id]->GetHandle(), NULL, NULL, RDW_INVALIDATE);
	return TRUE;
}

CXMainWindow::CXMainWindow(HINSTANCE hInstance, tstring szTitle, tstring szClass, int x, int y, int w, int h)
	: CXWindow(hInstance, MainWndProc),
	RadioGroup(this)
{
	SetText(szTitle);
	SetClass(szClass);
	SetPos(250, 125);
	SetWidth(w);
	SetHeight(h);
	SetStyle(WS_VISIBLE | WS_OVERLAPPEDWINDOW);
	hBgr = GetStockBrush(WHITE_BRUSH);
}


CXMainWindow::~CXMainWindow()
{
}


B1S p{ 2, L"test" };

bool CXMainWindow::Create()
{
	LoadLibrary(TEXT("Msftedit.dll")); // for rich text edit controls iirc
	INITCOMMONCONTROLSEX icc;
	icc.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icc.dwICC = ICC_TAB_CLASSES | ICC_LISTVIEW_CLASSES | ICC_LINK_CLASS;
	if (!InitCommonControlsEx(&icc))
		return false;

	WNDCLASSEX wcx = CreateBasicClass(szClass);
	RegisterClassEx(&wcx);
	if (!CreateXWindow())
		return false;
	
	xAgency = new CXFont(_T("Agency FB"), 20);
	xAgency->SetWeight(800);

	xVerdana = new CXFont(_T("Verdana"), 18);
	xVerdana->SetWeight(800);

	//Create controls
	pControls = new CXControls(this);
	g_pControls = pControls;
	//pControls->AddButton(IDBUTTON1, 5, 5, 100, 28, L"Testing", xAgency);
	pControls->AddControl<CXButton>(IDBUTTON1, 25, 5, 50, 28, L"", xAgency);
	std::function<void(uintptr_t)> pF = B1Wrap;
	reinterpret_cast<CXButton*>(g_pControls->mControls[IDBUTTON1])->SetAction(pF);
	g_pControls->GetControl<CXButton>(IDBUTTON1)->SetCommandArgs((uintptr_t)&p);

	HICON hIcon = (HICON)LoadImage(hInstance, L"icon.ico", IMAGE_ICON, LR_DEFAULTSIZE, LR_DEFAULTSIZE, LR_LOADFROMFILE);
	g_pControls->GetControl<CXButton>(IDBUTTON1)->SetIcon(hIcon);
	//HANDLE hBitmap = LoadImage(NULL, L"banner.bmp", IMAGE_BITMAP, LR_DEFAULTSIZE, LR_DEFAULTSIZE, LR_LOADFROMFILE);
	//g_pControls->GetControl<CXButton>(IDBUTTON1)->SetIcon((HICON)hBitmap);

	pControls->AddControl<CXEdit>(IDEDIT1, 115, 7, 200, 24, L"Edit Test");
	//pControls->AddEdit(IDEDIT1, 115, 7, 200, 24, L"Edit Test");
	//pControls->AddRadio(IDRADIO1, 5, 50, 100, 25, L"Semen");
	//pControls->AddRadio(IDRADIO2, 5, 80, 100, 25, L"Testes");
	pControls->AddCheckBox(IDCHECK1, 5, 110, 100, 25, L"WTEFF?!");


	////pControls->AddControl<CXGroupBox>(IDLISTBOX0, 105, 50, 300, 300, L"GroupBox0");
	////pControls->GetControl<CXGroupBox>(IDLISTBOX0)->pControls->AddControl<CXButton>(IDGROUP0RADIO0, 25, 25, 100, 25, L"GroupRadio");
	////pControls->GetControl<CXGroupBox>(IDLISTBOX0)->pControls->AddRadio(IDGROUP0RADIO0, 25, 25, 100, 25, L"GroupRadio");
	////pControls->GetControl<CXGroupBox>(IDLISTBOX0)->SetControlManager(g_pControls);
	////pControls->AddGroup(pControls->GetControl<CXGroupBox>(IDLISTBOX0));
	//pControls->AddGroup(0);
	////pControls->AddControlToGroup<CXRadioButton>(IDLISTBOX0, IDCHECK1, 4, 12, 100, 25, L"Testing");
	//
	//pControls->AddControlToGroup<CXRadioButton>(0, IDRADIO1, 5, 50, 100, 25, L"Semen");
	//pControls->AddControlToGroup<CXRadioButton>(0, IDRADIO2, 5, 80, 100, 25, L"Testes");
	////pControls->AddControlToGroup<CXCheckBox>(0, IDCHECK1, 5, 110, 100, 25, L"WTEFF?!");

	////pControls->GetControl<CXGroupBox>(IDLISTBOX0)->AddControl();
	////RECT rcGroup = pControls->GetControl<CXGroupBox>(IDLISTBOX0)->GetRect();
	////GetWindowRect(pControls->GetControl<CXGroupBox>(IDLISTBOX0)->GetHandle(), &rcGroup);
	////pControls->AddCheckBox(IDCHECK1, rcGroup.left + 15, rcGroup.top + 15, 100, 25, L"WTEFF?!");
	//
	////pRadio = new CXRadioButton(pControls->GetControl<CXGroupBox>(IDLISTBOX0), IDCHECK1, 5, 5, 100, 25, L"Testing");
	////pRadio->Create();
	////pControls->AddControl<CXRichText>(IDTABCONTROL0, 105, 50, 300, 300, L"Rich Edit");
	////pControls->GetControl<CXRichText>(IDTABCONTROL0)->SetBgColor(RGB(0, 255, 255));
	////pControls->GetControl<CXRichText>(IDTABCONTROL0)->SetTxtColor(RGB(0, 0, 0));
	//
	//////tabs are kinda fucked right now...
	////pControls->AddControl<CXTabControl>(IDTABCONTROL0, 155, 110, 300, 325, L"WTEwerFF?!", xVerdana);
	////pControls->GetControl<CXTabControl>(IDTABCONTROL0)->AddTab(L"Nothertab!");
	////pControls->GetControl<CXTabControl>(IDTABCONTROL0)->SetBgColor(this->clBgr);
	//
	pControls->AddControl<CXListView>(IDLISTBOX0, 5, 150, 275, 150, L"First Column");
	pControls->GetControl<CXListView>(IDLISTBOX0)->InsertColumn(L"Second Column");
	pControls->GetControl<CXListView>(IDLISTBOX0)->InsertColumn(L"Third Column");
	pControls->GetControl<CXListView>(IDLISTBOX0)->InsertColumn(L"Fourth Column");
	CXListViewItem lvi = CXListViewItem(0, 0, L"Item1", hIcon);
	lvi.AddSubItem(0, 1, L"Item2");
	lvi.AddSubItem(0, 2, L"Item3");
	pControls->GetControl<CXListView>(IDLISTBOX0)->AddItem(lvi);

	CXListViewItem lvi2 = CXListViewItem(1, 0, L"Item1", hIcon);
	lvi2.AddSubItem(1, 1, L"Item2");
	lvi2.AddSubItem(1, 2, L"Item3");
	pControls->GetControl<CXListView>(IDLISTBOX0)->AddItem(lvi2);
	//pControls->GetControl<CXListView>(IDLISTBOX0)->AddItem(0, 0);
	//pControls->GetControl<CXListView>(IDLISTBOX0)->AddItem(0, 1);
	//pControls->GetControl<CXListView>(IDLISTBOX0)->AddItem(0, 2);
	//pControls->GetControl<CXListView>(IDLISTBOX0)->AddItem(1, 0);
	//pControls->GetControl<CXListView>(IDLISTBOX0)->AddItem(1, 1);
	//pControls->GetControl<CXListView>(IDLISTBOX0)->AddItem(1, 2);

	//pControls->AddControl<CXComboBox>(IDCOMBO0, 150, 50, 100, 50, L"First Item");
	//pControls->GetControl<CXComboBox>(IDCOMBO0)->SetStyle(WS_XCOMBOBOXDROPLIST);

	//pControls->AddControl<CXTrackBar>(IDTRACKBAR0, 300, 200, 150, 50, L"fuk");

	//pControls->AddControl<CXSysLink>(IDSYSLINK0, 50, 380, 200, 25, L"<A HREF=\"http://google.com/\">Tis a link!</A>");

	pControls->AddControl<CXLabel>(IDLABEL1, 150, 250, 150, 25, L"Label1");
	//pControls->GetControl<CXLabel>(IDLABEL1)->SetTxtColor(RGB(255, 255, 255));
	return true;
}



void CXMainWindow::OnPaint()
{
	if((HBRUSH)GetClassLong(hWnd, GCL_HBRBACKGROUND) != hBgr)	//
		SetClassLong(hWnd, GCL_HBRBACKGROUND, (LONG)hBgr);		// i don't think i even need this... 
	
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	//do stuff.
	EndPaint(hWnd, &ps);
	
	bUpdate = false; //don't know if i'll really need this
}

LRESULT CXMainWindow::OnEraseBkgrnd(WPARAM wParam)
{
	return 1;
}
